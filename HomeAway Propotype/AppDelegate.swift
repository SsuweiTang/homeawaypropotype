//
//  AppDelegate.swift
//  HomeAway Propotype
//
//  Created by SophiaTang on 2017/8/1.
//  Copyright © 2017年 ASUS. All rights reserved.
//

import UIKit
import  CoreLocation

var geotifications: [Geotification] = []

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    let locationManager =  CLLocationManager ()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        //        let docDirectory: NSString = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0] as NSString
        //        let logpath = docDirectory.appendingPathComponent("LOG.txt")
        //        freopen(logpath.cString(using: String.Encoding.ascii)!, "a+", stderr)
        
        MyLog("AppDelegate ")
        locationManager.delegate  =  self
        locationManager.requestAlwaysAuthorization ()
        
        application.registerUserNotificationSettings ( UIUserNotificationSettings ( types : [.sound, .alert, .badge ], categories : nil ))
        UIApplication.shared.cancelAllLocalNotifications()
        
        self.window = UIWindow(frame:
            UIScreen.main.bounds)
        let nav = UINavigationController(
            rootViewController: ViewController())
        self.window!.rootViewController = nav
        self.window!.makeKeyAndVisible()
        
        return true
    }
    
    func sendEmail(_ data: Bool)
    {
        //        let mailto = ("sophia_tang@asus.com").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        var mailto = ("sophia_tang@asus.com").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
        if let _ = (UserDefaults.standard.string(forKey: "email"))
        {
            mailto = ((UserDefaults.standard.string(forKey: "email"))?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)!
        }
        
        var mailstring = "http://sgweb065-1.asuscomm.com:81/geofenceMail.php?mailto=\(mailto)&content=backHome"
        
        if data == false
        {
            mailstring = "http://sgweb065-1.asuscomm.com:81/geofenceMail.php?mailto=\(mailto)&content=levelHome"
        }
        
        let url = URL(string: mailstring)
        //using URLSession
        MyLog("AppDelegate sendEmail  mailstring \(mailstring)")
        
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "GET"
        
        
        var error: NSError? = nil
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue())
        let semaphore = DispatchSemaphore.init(value: 0)
        let task = session.dataTask(with: request as URLRequest, completionHandler: {replyData, response, errorData -> Void in
            if errorData == nil {
                
            }
            else {
                MyLog("HTTP Request Error = \(error)")
                
            }
            semaphore.signal()
        })
        
        task.resume()
        _ = semaphore.wait(timeout: .distantFuture)
        
        //    }
    }
    
    func  note ( fromRegionIdentifier  identifier : String ) ->  String? {
        let savedItems = UserDefaults.standard . array ( forKey : PreferencesKeys.savedItems ) as? [NSData]
        let geotifications = savedItems?.map { NSKeyedUnarchiver.unarchiveObject ( with : $0  as Data ) as? Geotification }
        let index = geotifications?.index { $0?.identifier == identifier }
        return index != nil ? geotifications?[index!]?.note : nil
    }
    
    func  handleEvent ( forRegion region : CLRegion! ,didEnter :Bool) {
        MyLog ("Geofence triggered!")
        
        DispatchQueue.global(qos: .default).async {
            if didEnter == true
            {
                self.sendEmail(true)
            }
            else
            {
                self.sendEmail(false)
            }
            
            if UIApplication.shared.applicationState  == .active {
                guard  let message =  self.note ( fromRegionIdentifier : region.identifier ) else { return }
                DispatchQueue.main.async{
                    self.window?.rootViewController?.showAlert ( withTitle : nil , message : message)
                }
            } else {
                let notification =  UILocalNotification ()
                notification.alertBody  =  self.note ( fromRegionIdentifier : region.identifier )
                notification.soundName  =  "Default "
                DispatchQueue.main.async{
                    UIApplication.shared.presentLocalNotificationNow (notification)
                }
            }
        }
    }
}

extension  AppDelegate : CLLocationManagerDelegate  ,URLSessionDataDelegate{
    
    func  locationManager ( _  manager : CLLocationManager, didEnterRegion  region : CLRegion) {
        if region is CLCircularRegion {
            MyLog("in  region  \(region)  ")
            handleEvent ( forRegion : region, didEnter : true)
        }
    }
    func  locationManager ( _  manager : CLLocationManager, didExitRegion  region : CLRegion) {
        if region is CLCircularRegion {
            MyLog("exit  region  \(region)")
            handleEvent ( forRegion : region, didEnter : false)
        }
    }
    
    
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void) {
        completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
}

public func MyLog(_ logString: String)
{
    let systemLog: Int = 2
    if systemLog == 1
    {
        print(logString)
    }
    else if systemLog == 2
    {
        NSLog("%@", logString)
    }
}

