//
//  ViewController.swift
//  HomeAway Propotype
//
//  Created by SophiaTang on 2017/8/1.
//  Copyright © 2017年 ASUS. All rights reserved.
//


import UIKit
import MapKit
import CoreLocation

struct PreferencesKeys {
    static let savedItems = "savedItems2"
}
class ViewController: UIViewController {
    
    var mapView: MKMapView! =  MKMapView()
    
    let locationManager =  CLLocationManager ()
    
    override open func viewWillAppear(_ animated: Bool) {
        
        MyLog("ViewController viewWillAppear")
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        loadAllGeotifications()
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        
        mapView.showsUserLocation = true
        MyLog("ViewController viewWillAppear geotifications  \(geotifications.count) \( CLLocationManager.locationServicesEnabled())")
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        MyLog("ViewController viewDidLoad")
        
        self.view.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        UINavigationBar.appearance().backgroundColor = UIColor.green
        self.title = "HomeAway"
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 14/255, green: 148/255, blue: 255/255, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent = true
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : AnyObject]
        navigationController?.navigationBar.tintColor = UIColor.white
        
        let leftButton = UIBarButtonItem(
            image: UIImage(named:"CurrentLocation"),
            style: .plain,
            target:self ,
            action: #selector(ViewController.zoomCurrentLocation))
        
        self.navigationItem.leftBarButtonItem = leftButton
        
        let rightButton = UIBarButtonItem(
            barButtonSystemItem: .add,
            target:self,
            action:#selector(ViewController.add2))
        self.navigationItem.rightBarButtonItem = rightButton
        
        
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.mapView)
        self.view.addConstraints([
            NSLayoutConstraint(item: mapView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: mapView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: mapView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: mapView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0)
            ])
        
        
        mapView.mapType = .standard
        mapView.delegate = self
        mapView.tintColor = UIColor(red: 102/255, green: 178/255, blue: 255/255, alpha: 1.0)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func zoomCurrentLocation()
    {
        MyLog("ViewController.swift zoomCurrentLocation ")
        mapView.zoomToUserLocation()
    }
    
    func add2()
    {
        self.navigationController!.pushViewController(AddGeotificationTableViewController(), animated: true)
    }
    
    func loadAllGeotifications() {
        MyLog("ViewController.swift loadAllGeotifications ")
        geotifications = []
        guard let savedItems = UserDefaults.standard.array(forKey: PreferencesKeys.savedItems) else { return }
        for savedItem in savedItems {
            guard let geotification = NSKeyedUnarchiver.unarchiveObject(with: savedItem as! Data) as? Geotification else { continue }
            add(geotification: geotification)
        }
    }
    
    func saveAllGeotifications() {
        MyLog("ViewController.swift saveAllGeotifications ")
        var items: [Data] = []
        for geotification in geotifications {
            let item = NSKeyedArchiver.archivedData(withRootObject: geotification)
            items.append(item)
        }
        UserDefaults.standard.set(items, forKey: PreferencesKeys.savedItems)
        
    }
    
    // MARK: Functions that update the model/associated views with geotification changes
    func add(geotification: Geotification) {
        MyLog("ViewController.swift add geotification  \(geotification)")
        geotifications.append(geotification)
        mapView.addAnnotation(geotification)
        MyLog("ViewController.swift add geotifications.count \(geotifications.count)")
        
        addRadiusOverlay(forGeotification: geotification)
        updateGeotificationsCount()
    }
    
    func remove(geotification: Geotification) {
        if let indexInArray = geotifications.index(of: geotification) {
            geotifications.remove(at: indexInArray)
        }
        mapView.removeAnnotation(geotification)
        removeRadiusOverlay(forGeotification: geotification)
        updateGeotificationsCount()
    }
    
    func updateGeotificationsCount() {
        MyLog("ViewController.swift  updateGeotificationsCount  \(geotifications.count)")
        title = "HomeAway (\(geotifications.count))"
        navigationItem.rightBarButtonItem?.isEnabled  = (geotifications.count  <  20 )
    }
    
    // MARK: Map overlay functions
    func addRadiusOverlay(forGeotification geotification: Geotification) {
        mapView.add(MKCircle(center: geotification.coordinate, radius: geotification.radius))
    }
    
    func removeRadiusOverlay(forGeotification geotification: Geotification) {
        // Find exactly one overlay which has the same coordinates & radius to remove
        guard let overlays = mapView?.overlays else { return }
        for overlay in overlays {
            guard let circleOverlay = overlay as? MKCircle else { continue }
            let coord = circleOverlay.coordinate
            if coord.latitude == geotification.coordinate.latitude && coord.longitude == geotification.coordinate.longitude && circleOverlay.radius == geotification.radius {
                mapView?.remove(circleOverlay)
                break
            }
        }
    }
}

// MARK: - Location Manager Delegate
extension ViewController: CLLocationManagerDelegate {
    func  locationManager ( _  manager : CLLocationManager, didChangeAuthorization  status : CLAuthorizationStatus) {
        mapView.showsUserLocation = (status == .authorizedAlways )
    }
}

// MARK: - MapView Delegate
extension ViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "myGeotification"
        if annotation is Geotification {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = true
                let removeButton = UIButton(type: .custom)
                removeButton.frame = CGRect(x: 0, y: 0, width: 23, height: 23)
                removeButton.setImage(UIImage(named: "DeleteGeotification")!, for: .normal)
                annotationView?.leftCalloutAccessoryView = removeButton
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            circleRenderer.strokeColor = .purple
            circleRenderer.fillColor = UIColor.purple.withAlphaComponent(0.4)
            return circleRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        // Delete geotification
        let geotification = view.annotation as! Geotification
        stopMonitoring ( geotification : geotification)
        remove(geotification: geotification)
        saveAllGeotifications()
    }
    
    
    func  region ( withGeotification  geotification : Geotification) -> CLCircularRegion {
        let region = CLCircularRegion(center:geotification.coordinate,radius:geotification.radius ,identifier:geotification.identifier )
        
        region.notifyOnEntry = (geotification.eventType == .onEntry )
        region.notifyOnExit = !region.notifyOnEntry
        return region
    }
    
    func startMonitoring ( geotification : Geotification) {
        if  !CLLocationManager.isMonitoringAvailable ( for : CLCircularRegion.self ) {
            showAlert ( withTitle : " Error " , message : "Geofencing is not supported on this device! " )
            return
        }
        if CLLocationManager.authorizationStatus () != .authorizedAlways {
            showAlert ( withTitle : " Warning " , message : "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location. " )
        }
        let region =  self.region ( withGeotification : geotification)
        MyLog("startMonitoring  region  \(region)")
        locationManager.startMonitoring ( for : region)
    }
    
    func  stopMonitoring ( geotification : Geotification) {
        for region in locationManager.monitoredRegions {
            guard  let circularRegion = region as? CLCircularRegion, circularRegion.identifier  == geotification.identifier  else { continue }
            locationManager.stopMonitoring ( for : circularRegion)
        }
    }
    
    
    func  locationManager ( _  manager : CLLocationManager, monitoringDidFailFor region : CLRegion? , withError  error : Error ) {
        MyLog ("Monitoring failed for region with identifier: \( region!.identifier ) " )
    }
    
    func  locationManager ( _  manager : CLLocationManager, didFailWithError error : Error ) {
        MyLog ("Location Manager failed with the following error: \( error ) " )
    }
}

// MARK: AddGeotificationViewControllerDelegate
extension ViewController: AddGeotificationTableViewControllerDelegate {
    
    func addGeotificationTableViewController(controller: AddGeotificationTableViewController, didAddCoordinate coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, note: String, eventType: EventType) {
        //    controller.dismiss(animated: true, completion: nil)
        //    let geotification = Geotification(coordinate: coordinate, radius: radius, identifier: identifier, note: note, eventType: eventType)
        //    add(geotification: geotification)
        //    saveAllGeotifications()
        
        let clampedRadius =  min (radius,locationManager.maximumRegionMonitoringDistance )
        let geotification =  Geotification ( coordinate : coordinate, radius : clampedRadius, identifier : identifier, note : note, eventType : eventType)
        add ( geotification : geotification)
        
        startMonitoring ( geotification : geotification)
        saveAllGeotifications ()
    }
    
}

