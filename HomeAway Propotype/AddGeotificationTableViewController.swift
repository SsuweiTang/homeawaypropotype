//
//  AddGeotificationTableViewController.swift
//  HomeAway Propotype
//
//  Created by SophiaTang on 2017/8/1.
//  Copyright © 2017年 ASUS. All rights reserved.
//

import UIKit
import MessageUI
import MapKit
import CoreLocation


protocol AddGeotificationTableViewControllerDelegate {
    func addGeotificationTableViewController(controller: AddGeotificationTableViewController, didAddCoordinate coordinate: CLLocationCoordinate2D,
                                             radius: Double, identifier: String, note: String, eventType: EventType)
}
class AddGeotificationTableViewController: UITableViewController, UITextFieldDelegate,  AddGeotificationTableViewControllerDelegate {

    var mapCell: UITableViewCell = UITableViewCell()
    var mapView: MKMapView! =  MKMapView()
    
    var addPinImageView :UIImageView = UIImageView()
    var radiusCell = UITableViewCell()
    var radiusLabel = UILabel()
    var radiusTextField = UITextField()
    
    var noteCell = UITableViewCell()
    var noteLabel = UILabel()
    var noteTextField = UITextField()
    let locationManager =  CLLocationManager ()

    var emailCell = UITableViewCell()
    var emailLabel = UILabel()
    var emailTextField = UITextField()
    
    var eventSegmentedControl:UISegmentedControl = UISegmentedControl ()
    var leftButton :UIBarButtonItem =  UIBarButtonItem()
    var addButton :UIBarButtonItem =  UIBarButtonItem()
    var zoomButton :UIBarButtonItem =  UIBarButtonItem()
    var Adddelegate: AddGeotificationTableViewControllerDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        MyLog("AddGeotificationTableViewController.swift viewWillAppear")
        super.viewWillAppear(animated)
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest

         locationManager.startUpdatingLocation()
        addButton.isEnabled = false
        AddGeotificationTableViewController().Adddelegate = self
        AddGeotificationTableViewController().Adddelegate = ViewController()
    }
    
    
    
    func cancel ()
    {
        _ = navigationController?.popToRootViewController(animated: true)
        
    }
    
    func add ()
    {
         MyLog("AddGeotificationTableViewController.swift add")
        let coordinate = mapView.centerCoordinate
        let radius = Double(radiusTextField.text!) ?? 0
        let identifier = NSUUID().uuidString
        let note = noteTextField.text
        let eventType: EventType = (eventSegmentedControl.selectedSegmentIndex == 0) ? .onEntry : .onExit
        MyLog("AddGeotificationTableViewController.swift  onAdd  latitude \(coordinate.latitude)  longitude\(coordinate.longitude) radius\(radius) identifier\(identifier)  note\(note)  eventType\(eventType)")
        //        Adddelegate?.addGeotificationTableViewController(controller: AddGeotificationTableViewController(), didAddCoordinate: coordinate, radius: radius, identifier: identifier, note: note!, eventType: eventType)
        let clampedRadius =  min (radius,locationManager.maximumRegionMonitoringDistance )
        let geotification =  Geotification ( coordinate : coordinate, radius : clampedRadius, identifier : identifier, note : note!, eventType : eventType)
        ViewController().add ( geotification : geotification)
        ViewController().startMonitoring ( geotification : geotification)
        ViewController().saveAllGeotifications ()
        UserDefaults.standard.set(emailTextField.text, forKey: "email")
        MyLog("AddGeotificationTableViewController.swift email\(emailTextField.text) ")
        
    }

    func setLeftBarItem()
    {
        MyLog("AddGeotificationTableViewController.swift  setLeftBarItem")
        leftButton =  UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(cancel))
        self.navigationItem.leftBarButtonItem = leftButton
        
    }
    
    func zoomCurrentLocation()
    {
        MyLog("AddGeotificationTableViewController.swift  zoomCurrentLocation")
        mapView.zoomToUserLocation()
    }
    
    func setRightBarItem()
    {
        MyLog("AddGeotificationTableViewController.swift  setRightBarItem")
        
        addButton =  UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(add))
        zoomButton = UIBarButtonItem(
            image: UIImage(named:"CurrentLocation"),
            style:.plain ,
            target:self ,
            action: #selector(self.zoomCurrentLocation))
        
        
        self.navigationItem.rightBarButtonItems = [addButton,zoomButton]
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        MyLog("AddGeotificationTableViewController.swift viewDidLoad")

        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = titleDict as? [String : AnyObject]
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor(red: 14/255, green: 148/255, blue: 255/255, alpha: 1.0)
        navigationController?.navigationBar.tintColor = UIColor.white
         self.title = "HomeAway"
        
        self.setLeftBarItem()
        self.setRightBarItem()

        eventSegmentedControl = UISegmentedControl(
            items: ["Upon Entry","Upon Exit"])
        eventSegmentedControl.tintColor = UIColor(red: 41/255, green: 148/255, blue: 255/255, alpha: 1.0)
        eventSegmentedControl.backgroundColor = UIColor.white
        eventSegmentedControl.selectedSegmentIndex = 0

        self.eventSegmentedControl.translatesAutoresizingMaskIntoConstraints = false
        self.mapCell.addSubview(self.eventSegmentedControl)
        self.mapCell.addConstraints([
            NSLayoutConstraint(item: eventSegmentedControl, attribute: .top, relatedBy: .equal, toItem: self.mapCell, attribute: .top, multiplier: 1.0, constant:5),
            NSLayoutConstraint(item: eventSegmentedControl, attribute: .right, relatedBy: .equal, toItem: self.mapCell, attribute: .right, multiplier: 1.0, constant: -10),
            NSLayoutConstraint(item: eventSegmentedControl, attribute: .left, relatedBy: .equal, toItem: self.mapCell, attribute: .left, multiplier: 1.0, constant: 10),
            ])
        
        
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.mapCell.addSubview(self.mapView)
        self.mapCell.addConstraints([
            NSLayoutConstraint(item: mapView, attribute: .top, relatedBy: .equal, toItem: self.eventSegmentedControl, attribute: .bottom, multiplier: 1.0, constant: 5),
            NSLayoutConstraint(item: mapView, attribute: .right, relatedBy: .equal, toItem: self.mapCell, attribute: .right, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: mapView, attribute: .left, relatedBy: .equal, toItem: self.mapCell, attribute: .left, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: mapView, attribute: .bottom, relatedBy: .equal, toItem: self.mapCell, attribute: .bottom, multiplier: 1.0, constant: 0)
            ])
        
        
        mapView.mapType = .standard
        mapView.delegate =  self
        mapView.tintColor = UIColor(red: 102/255, green: 178/255, blue: 255/255, alpha: 1.0)
        mapCell.selectionStyle = UITableViewCellSelectionStyle.none

        self.radiusLabel.text = "Radius"
        self.radiusLabel.textAlignment = .left
        self.radiusLabel.translatesAutoresizingMaskIntoConstraints = false
        self.radiusTextField.translatesAutoresizingMaskIntoConstraints = false
        self.radiusTextField.textAlignment = .left
        self.radiusTextField.delegate = self
        radiusTextField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        radiusTextField.placeholder = "Radius in meters"
        
        self.radiusTextField.text = "100"
        radiusTextField.keyboardType = .numberPad
        self.radiusCell.addSubview(self.radiusLabel)
        self.radiusCell.addSubview(self.radiusTextField)
        self.radiusCell.addConstraints([
            NSLayoutConstraint(item: self.radiusLabel, attribute: .left, relatedBy: .equal, toItem: self.radiusCell, attribute: .left, multiplier: 1.0, constant: 15),
            NSLayoutConstraint(item: self.radiusLabel, attribute: .top, relatedBy: .equal, toItem: self.radiusCell, attribute: .top, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.radiusLabel, attribute: .height, relatedBy: .equal, toItem: self.radiusCell, attribute: .height, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.radiusLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: self.radiusCell.frame.width / 3)
            ])
        self.radiusCell.addConstraints([
            NSLayoutConstraint(item: self.radiusTextField, attribute: .left, relatedBy: .equal, toItem: self.radiusLabel, attribute: .right, multiplier: 1.0, constant: -20),
            NSLayoutConstraint(item: self.radiusTextField, attribute: .top, relatedBy: .equal, toItem: self.radiusCell, attribute: .top, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.radiusTextField, attribute: .height, relatedBy: .equal, toItem: self.radiusCell, attribute: .height, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.radiusTextField, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: self.radiusCell.frame.width / 3*2)
            ])
        
        
        self.noteLabel.text = "Note"
        self.noteLabel.textAlignment = .left
        self.noteLabel.translatesAutoresizingMaskIntoConstraints = false
        self.noteTextField.translatesAutoresizingMaskIntoConstraints = false
        self.noteTextField.textAlignment = .left
        self.noteTextField.delegate = self
        noteTextField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        noteTextField.placeholder = "Reminder note to be shown"
        self.noteCell.addSubview(self.noteLabel)
        self.noteCell.addSubview(self.noteTextField)
        self.noteCell.addConstraints([
            NSLayoutConstraint(item: self.noteLabel, attribute: .left, relatedBy: .equal, toItem: self.noteCell, attribute: .left, multiplier: 1.0, constant: 15),
            NSLayoutConstraint(item: self.noteLabel, attribute: .top, relatedBy: .equal, toItem: self.noteCell, attribute: .top, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.noteLabel, attribute: .height, relatedBy: .equal, toItem: self.noteCell, attribute: .height, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.noteLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: self.noteCell.frame.width / 3)
            ])
        self.noteCell.addConstraints([
            NSLayoutConstraint(item: self.noteTextField, attribute: .left, relatedBy: .equal, toItem: self.noteLabel, attribute: .right, multiplier: 1.0, constant: -20),
            NSLayoutConstraint(item: self.noteTextField, attribute: .top, relatedBy: .equal, toItem: self.noteCell, attribute: .top, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.noteTextField, attribute: .height, relatedBy: .equal, toItem: self.noteCell, attribute: .height, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.noteTextField, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: self.noteCell.frame.width / 3*2)
            ])

        self.emailLabel.text = "Email"
        self.emailLabel.textAlignment = .left
        self.emailLabel.translatesAutoresizingMaskIntoConstraints = false
        self.emailTextField.translatesAutoresizingMaskIntoConstraints = false
        self.emailTextField.textAlignment = .left
        self.emailTextField.delegate = self
        emailTextField.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        //        self.noteTextField.isUserInteractionEnabled = false
        emailTextField.placeholder = "Enter mail"
        self.emailCell.addSubview(self.emailLabel)
        self.emailCell.addSubview(self.emailTextField)
        self.emailCell.addConstraints([
            NSLayoutConstraint(item: self.emailLabel, attribute: .left, relatedBy: .equal, toItem: self.emailCell, attribute: .left, multiplier: 1.0, constant: 15),
            NSLayoutConstraint(item: self.emailLabel, attribute: .top, relatedBy: .equal, toItem: self.emailCell, attribute: .top, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.emailLabel, attribute: .height, relatedBy: .equal, toItem: self.emailCell, attribute: .height, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.emailLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: self.emailCell.frame.width / 3)
            ])
        self.emailCell.addConstraints([
            NSLayoutConstraint(item: self.emailTextField, attribute: .left, relatedBy: .equal, toItem: self.emailLabel, attribute: .right, multiplier: 1.0, constant: -20),
            NSLayoutConstraint(item: self.emailTextField, attribute: .top, relatedBy: .equal, toItem: self.emailCell, attribute: .top, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.emailTextField, attribute: .height, relatedBy: .equal, toItem: self.emailCell, attribute: .height, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: self.emailTextField, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: self.emailCell.frame.width / 3*2)
            ])

        emailTextField.text = "Sophia_tang@asus.com"
        self.addPinImageView.translatesAutoresizingMaskIntoConstraints = false
        self.addPinImageView.image = UIImage(named: "AddPin")
        self.mapCell.addSubview(addPinImageView)
        self.mapCell.addConstraints([
            NSLayoutConstraint(item: addPinImageView, attribute: .centerX, relatedBy: .equal, toItem: self.mapView, attribute: .centerX, multiplier: 1.0, constant:
                0),
            NSLayoutConstraint(item: addPinImageView, attribute: .centerY, relatedBy: .equal, toItem: self.mapView, attribute: .centerY, multiplier: 1.0, constant: 0),
            
            ])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
//        return NSLocalizedString("HomeAway", comment: "")
        return "Radius in meters"
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0
        {
            return self.view.frame.height/3*2
        }
        return self.radiusCell.frame.height
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch(section) {
        case 0: return 4
            
        default: fatalError("Unknown number of sections")
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0: return self.mapCell
        case 1: return self.radiusCell
        case 2: return self.noteCell
        case 3: return self.emailCell
        default:
            fatalError("Unknown row in section 0")
        }
    }
    
    // Configure the row selection code for any cells that you want to customize the row selection
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldEditingChanged(_ textField: UITextField) {
        addButton.isEnabled = !radiusTextField.text!.isEmpty && !noteTextField.text!.isEmpty
    }
    
    
    func addGeotificationTableViewController(controller: AddGeotificationTableViewController, didAddCoordinate coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, note: String, eventType: EventType) {

        let clampedRadius =  min (radius,locationManager.maximumRegionMonitoringDistance )
        let geotification =  Geotification ( coordinate : coordinate, radius : clampedRadius, identifier : identifier, note : note, eventType : eventType)
        ViewController().add ( geotification : geotification)
        ViewController().startMonitoring ( geotification : geotification)
        ViewController().saveAllGeotifications ()
    }

}


// MARK: - Location Manager Delegate
extension AddGeotificationTableViewController: CLLocationManagerDelegate {
    func  locationManager ( _  manager : CLLocationManager, didChangeAuthorization  status : CLAuthorizationStatus) {
        mapView.showsUserLocation = (status == .authorizedAlways )
    }
    
}

// MARK: - MapView Delegate
extension AddGeotificationTableViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "myGeotification"
        if annotation is Geotification {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView?.canShowCallout = true
                let removeButton = UIButton(type: .custom)
                removeButton.frame = CGRect(x: 0, y: 0, width: 23, height: 23)
                removeButton.setImage(UIImage(named: "DeleteGeotification")!, for: .normal)
                annotationView?.leftCalloutAccessoryView = removeButton
            } else {
                annotationView?.annotation = annotation
            }
            return annotationView
        }
        return nil
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            circleRenderer.strokeColor = .purple
            circleRenderer.fillColor = UIColor.purple.withAlphaComponent(0.4)
            return circleRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        // Delete geotification
        let geotification = view.annotation as! Geotification
        ViewController().stopMonitoring ( geotification : geotification)
        ViewController().remove(geotification: geotification)
        ViewController().saveAllGeotifications()
    }
    
    
    func  region ( withGeotification  geotification : Geotification) -> CLCircularRegion {
        let region = CLCircularRegion(center:geotification.coordinate,radius:geotification.radius ,identifier:geotification.identifier )
        region.notifyOnEntry = (geotification.eventType == .onEntry )
        region.notifyOnExit = !region.notifyOnEntry
        return region
    }

    func  locationManager ( _  manager : CLLocationManager, monitoringDidFailFor region : CLRegion? , withError  error : Error ) {
        print ( " Monitoring failed for region with identifier: \( region!.identifier ) " )
    }
    
    func  locationManager ( _  manager : CLLocationManager, didFailWithError error : Error ) {
        print ( " Location Manager failed with the following error: \( error ) " )
    }
}

//extension AddGeotificationTableViewController: AddGeotificationTableViewControllerDelegate {
//    func addGeotificationTableViewController(controller: AddGeotificationTableViewController, didAddCoordinate coordinate: CLLocationCoordinate2D, radius: Double, identifier: String, note: String, eventType: EventType) {
//
//        let clampedRadius =  min (radius,locationManager.maximumRegionMonitoringDistance )
//        let geotification =  Geotification ( coordinate : coordinate, radius : clampedRadius, identifier : identifier, note : note, eventType : eventType)
//        ViewController().add ( geotification : geotification)
//        print("===2222  AddGeotificationTableViewController ")
//        ViewController().startMonitoring ( geotification : geotification)
//        ViewController().saveAllGeotifications ()
//    }
//    
//}


